import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
import * as firebase from 'firebase/app';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {
  account = {
    email: '',
    password: '',
    password2: ''
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private afAuth: AngularFireAuth
  ) {
  }

  ngOnInit() {
    this.account = {
      email: '',
      password: '',
      password2: ''
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  displayAlert(alertTitle, alertSub) {
    let theAlert = this.alertCtrl.create({
      title: alertTitle,
      subTitle: alertSub,
      buttons: ['OK']
    });
    theAlert.present();
  }

  registerSuccess(res) {
    this.displayAlert('Register', 'Success');
    this.navCtrl.push(HomePage);
  }
  registerAccount() {
    if (this.account.password != this.account.password2) {
      this.displayAlert('Password Problem', 'Passwords do not match');
      this.account.password2 = '';
      this.account.password = '';
    } else {
      this.afAuth.auth.createUserWithEmailAndPassword(this.account.email, this.account.password)
        .then(res => this.registerSuccess(res))
        .catch(err => this.displayAlert('Error', err));
    }
  }

}
