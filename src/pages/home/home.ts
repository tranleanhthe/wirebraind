import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { CatelogyPage } from '../catelogy/catelogy';
import { AccountPage } from '../account/account';
import { AboutPage } from '../about/about';
import { LocationPage } from '../location/location';
import { AboutMePage } from '../about-me/about-me';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  colorToolbar: any = 'secondary';

  menuData = [
    { title: 'Menu', pic: 'assets/img/soup1.jpg', pushPage: 'CatelogyPage' },
    { title: 'Account', pic: 'assets/img/coffee-people3.jpg', pushPage: 'AccountPage' },
    { title: 'About Us', pic: 'assets/img/coffee6.jpg', pushPage: 'AboutPage' },
    { title: 'Location', pic: 'assets/img/cafe2.jpg', pushPage: 'LocationPage' }
  ];
  loggedIn: any = null;
  logPage: any;

  constructor(
    public navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private userService: UserServiceProvider) { }

  ngOnInit() {
    this.logPage = 'LoginPage';
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.loggedIn = user.email;
      }
    })
  }

  signOff() {
    this.userService.logOut();
    this.loggedIn = null;
  }

  aboutMePage() {
    this.navCtrl.push(AboutMePage);
  }

  myPagePush(value?) {
    if (value === 'CatelogyPage') {
      this.navCtrl.push(CatelogyPage);
    }
    if (value == 'AccountPage') {
      this.navCtrl.push(AccountPage);
    }
    if (value == 'AboutPage') {
      this.navCtrl.push(AboutPage);
    }
    if (value == 'LocationPage') {
      this.navCtrl.push(LocationPage);
    }
  }
}
