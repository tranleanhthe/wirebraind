import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { HomePage } from '../home/home';
import { leave } from '@angular/core/src/profile/wtf_impl';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {
  login = {
    email: '',
    password: ''
  }
  registerPage: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userSerivce: UserServiceProvider) {
  }

  ionViewDidLoad() {
  }

  ngOnInit() {
    this.registerPage = 'RegisterPage';
  }

  signOn() {
    if (!this.login.email || !this.login.password) {
      this.userSerivce.displayAlert('Error', 'You must enter email and password');
    } else {
      this.userSerivce.logOn(this.login.email, this.login.password)
        .then(res => {
          if (this.userSerivce.success) {
            this.navCtrl.push(HomePage);
          } else {
            this.login.email = '';
            this.login.password = '';
          }
        })
    }
  }

}
