import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CatelogyPage } from './catelogy';

@NgModule({
  declarations: [
    CatelogyPage,
  ],
  imports: [
    IonicPageModule.forChild(CatelogyPage),
  ],
})
export class CatelogyPageModule {}
