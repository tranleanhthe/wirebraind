import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserServiceProvider {

  items: FirebaseListObservable<any>;
  user: string;
  success: boolean;
  constructor(
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    private storage: Storage,
    private fbDb: AngularFireDatabase) {
    this.items = fbDb.list('/users');
  }

  displayAlert(alertTitle, alertSub) {
    let theAlert = this.alertCtrl.create({
      title: alertTitle,
      subTitle: alertSub,
      buttons: ['OK']
    });
    theAlert.present();
  }

  isLogin() {
    return this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        return true;
      } else {
        return false;
      }
    });
  }

  logOn(user, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(user, password)
      .then(result => {
        if (result) {
          this.storageControl('get', user)
            .then(returned => {
              if (!returned) {
                this.saveNewUser(user)
                  .then(res => {

                  })
              } else {
                this.success = true;
                return result;
              }
            })
          this.success = true;
          return result;
        }
      })
      .catch(err => {
        this.success = false;
        this.displayAlert('Error loggin in', err);
        return err;
      })
  }

  logOut() {
    this.afAuth.auth.signOut();
  }

  storageControl(action, key?, value?) {
    if (action == 'set') {
      return this.storage.set(key, value);
    }
    if (action == 'get') {
      return this.storage.get(key);
    }
    if (action == 'delete') {
      if (!key) {
        this.displayAlert('Warning', 'About to delete all user data');
        return this.storage.clear();
      } else {
        this.displayAlert(key, 'Deleting this user data');
        return this.storage.remove(key);
      }
    }
  }

  saveNewUser(user) {
    let userObj = {
      creation: new Date().toDateString(),
      logins: 1,
      rewardCount: 0,
      lastLogin: new Date().toLocaleDateString(),
      id: ''
    }

    this.items.push({
      username: user,
      creation: userObj.creation,
      logins: userObj.logins,
      rewardCount: userObj.rewardCount,
      lastLogin: userObj.lastLogin
    }).then(res => {
      userObj.id = res.key;
      return this.storageControl('set', user, userObj);
    })
    return this.storageControl('get', user);
  }

}
