import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import * as firebase from "firebase";
import { UserServiceProvider } from '../providers/user-service/user-service';
import { IonicStorageModule } from '@ionic/storage';
import { MenuController } from 'ionic-angular';
import { AccountPage } from '../pages/account/account';
import { CatelogyPage } from '../pages/catelogy/catelogy';
import { AboutPage } from '../pages/about/about';
import { LocationPage } from '../pages/location/location';
import { AboutMePage } from '../pages/about-me/about-me';

export const firebaseConfig = {
  apiKey: "AIzaSyCwXisZOYjICgKuphsOXEeaotAS53TE1W0",
  authDomain: "demoapp-6b2cd.firebaseapp.com",
  databaseURL: "https://demoapp-6b2cd.firebaseio.com",
  projectId: "demoapp-6b2cd",
  storageBucket: "demoapp-6b2cd.appspot.com",
  messagingSenderId: "425584232136"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AccountPage,
    CatelogyPage,
    AboutPage,
    LocationPage,
    AboutMePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AccountPage,
    CatelogyPage,
    AboutPage,
    LocationPage,
    AboutMePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserServiceProvider
  ]
})
export class AppModule { }
